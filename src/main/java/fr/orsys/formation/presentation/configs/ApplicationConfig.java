package fr.orsys.formation.presentation.configs;

import fr.orsys.formation.presentation.interceptors.LogInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfig {

    @Bean
    public LogInterceptor logInterceptor() {
        return new LogInterceptor();
    }
}
