package fr.orsys.formation.presentation.services.libraire;

import fr.orsys.formation.presentation.models.entities.Libraire;
import fr.orsys.formation.presentation.services.CrudService;

public interface LibraireService extends CrudService<Libraire, Integer> {
}
