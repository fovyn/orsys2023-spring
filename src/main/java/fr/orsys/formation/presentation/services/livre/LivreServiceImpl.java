package fr.orsys.formation.presentation.services.livre;

import fr.orsys.formation.presentation.models.entities.Livre;
import fr.orsys.formation.presentation.repositories.LivreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.stream.Stream;

@Service
public class LivreServiceImpl implements LivreService {
    private final LivreRepository repository;

    @Autowired
    public LivreServiceImpl(LivreRepository repository) {
        this.repository = repository;
    }

    @Override
    public Livre create(Livre entity) {

        return this.repository.save(entity);
    }

    @Override
    public Optional<Livre> readOne(Long aLong) {
        return this.repository.findById(aLong);
    }

    public Optional<Livre> readOne(String isbn) {
        return this.repository.findByIsbn(isbn);
    }

    @Override
    public Stream<Livre> readAll() {
        return this.repository.findAll().stream();
    }

    @Transactional
    @Override
    public Livre update(Long aLong, Livre entity) {
        Livre toUpdate = this.repository.findById(aLong).orElseThrow();

        toUpdate.setIsbn(entity.getIsbn());
        toUpdate.setTitre(entity.getTitre());
        toUpdate.setRecompenses(entity.getRecompenses());

        return this.repository.save(toUpdate);
    }

    @Override
    public void delete(Livre entity) {
        this.repository.delete(entity);
    }

    @Override
    public void delete(Long aLong) {
        this.repository.deleteById(aLong);
    }
}
