package fr.orsys.formation.presentation.repositories;

import fr.orsys.formation.presentation.models.entities.Livre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LivreRepository extends JpaRepository<Livre, Long> {

    @Query(value = "SELECT l FROM Livre l WHERE l.isbn = :isbn")
    Optional<Livre> findByIsbn(@Param("isbn") String isbn);

    @Modifying
    @Query(value = "UPDATE Livre l SET l.titre = :titre WHERE l.isbn = :isbn")
    Livre updateTitle(@Param("titre") String titre, @Param("isbn") String isbn);


    @Procedure(procedureName = "findAll")
    List<Livre> callFindAll();
}
