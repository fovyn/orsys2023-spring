package fr.orsys.formation.presentation.models.entities;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@RequiredArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@NoArgsConstructor
//@Builder
@Entity
public class Edition implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private Long id;
    @Getter
    @Setter
    private int ordre;
    @Getter
    @Setter
    @NonNull
    private LocalDate date;
    @Getter
    @Setter
    private float prix;

    @ManyToOne(targetEntity = Livre.class)
    private Livre livre;
}
