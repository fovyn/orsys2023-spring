package fr.orsys.formation.presentation.services;

import java.io.Serializable;
import java.util.Optional;
import java.util.stream.Stream;

public interface CrudService<T extends Serializable, TKey> {
    T create(T entity);

    Optional<T> readOne(TKey key);

    Stream<T> readAll();

    T update(TKey key, T entity);

    void delete(T entity);

    void delete(TKey key);
}
