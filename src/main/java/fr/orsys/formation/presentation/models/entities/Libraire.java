package fr.orsys.formation.presentation.models.entities;

import lombok.*;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = true)
@ToString
@Entity
public class Libraire extends Personne implements Serializable {
    @Getter
    @Setter
    @Embedded
    private Adresse adresse;
}
