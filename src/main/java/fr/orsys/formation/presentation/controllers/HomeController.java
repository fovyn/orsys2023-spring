package fr.orsys.formation.presentation.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
    //    private final ClientService clientService;
//
//    public HomeController(ClientService clientService) {
//        this.clientService = clientService;
//    }
//
    @GetMapping
    public String indexAction() {
        return "home/index";
    }
}
