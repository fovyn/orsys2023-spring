package fr.orsys.formation.presentation.models.entities;

public enum CommandState {
    EnPreparation(() -> 1),
    Preparer(() -> 2),
    Envoyer(() -> 3),
    Cloturer(() -> 3);

    private final OperationStrategy<Integer> operationStrategy;

    CommandState(OperationStrategy<Integer> operationStrategy) {
        this.operationStrategy = operationStrategy;
    }

    public CommandState nextState() {
        return CommandState.values()[operationStrategy.compute()];
    }
}
