package fr.orsys.formation.presentation.models.entities;

import lombok.*;

import java.io.Serializable;

@RequiredArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
//@Builder
public class CommandeEdition implements Serializable {
    @Getter
    @Setter
    private Edition edition;
    @Getter
    @Setter
    @NonNull
    private int qtt;
}
