package fr.orsys.formation.presentation.services.livre;

import fr.orsys.formation.presentation.models.entities.Livre;
import fr.orsys.formation.presentation.services.CrudService;

import java.util.Optional;

public interface LivreService extends CrudService<Livre, Long> {
    Optional<Livre> readOne(String isbn);
}
