package fr.orsys.formation.presentation.models.entities;

@FunctionalInterface
public interface OperationStrategy<TResult> {
    TResult compute();
}
