package fr.orsys.formation.presentation.services.commande;

import fr.orsys.formation.presentation.models.entities.Commande;
import fr.orsys.formation.presentation.services.CrudService;

public interface CommandeService extends CrudService<Commande, Long> {
}
