package fr.orsys.formation.presentation.models.entities;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@RequiredArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@NoArgsConstructor
//@Builder
@Entity
public class Livre implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Getter
    @Setter
    @NonNull
    @Column(nullable = false, length = 50)
    private String isbn;

    @Getter
    @Setter
    @NonNull
    private String titre;

    @Getter
    @Setter
    @Singular
    @ElementCollection(fetch = FetchType.LAZY)
    private List<String> recompenses = newArrayList();


    @OneToMany(targetEntity = Edition.class, mappedBy = "livre")
    private List<Edition> editions;
}
