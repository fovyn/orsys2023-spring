package fr.orsys.formation.presentation.services.libraire;

import fr.orsys.formation.presentation.models.entities.Libraire;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static com.google.common.collect.Lists.newArrayList;

@Service
public class LibraireServiceImpl implements LibraireService {
    private final List<Libraire> libraires = newArrayList();

    @Override
    public Libraire create(Libraire entity) {
        if (libraires.contains(entity)) throw new RuntimeException("Library already exists");

//        entity.setId(libraires.size() + 1);
        libraires.add(entity);

        return entity;
    }

    @Override
    public Optional<Libraire> readOne(Integer integer) {
//        return this.libraires.stream().filter(it -> it.getId().equals(integer)).findFirst();
        return Optional.empty();
    }

    @Override
    public Stream<Libraire> readAll() {
        return this.libraires.stream();
    }

    @Override
    public Libraire update(Integer integer, Libraire entity) {
        Optional<Libraire> opt = readOne(integer);

        Libraire libraire = opt.orElseThrow(() -> new RuntimeException("Libraire doesn't exists"));

        libraire.setNom(entity.getNom());
        libraire.setAdresse(entity.getAdresse());

        return libraire;
    }

    @Override
    public void delete(Libraire entity) {
        this.libraires.remove(entity);
    }

    @Override
    public void delete(Integer integer) {
        Libraire libraire = readOne(integer).orElseThrow(() -> new RuntimeException("Libraire doesn't exists"));

        this.libraires.remove(libraire);
    }
}
