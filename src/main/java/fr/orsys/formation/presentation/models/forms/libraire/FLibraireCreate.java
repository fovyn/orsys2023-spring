package fr.orsys.formation.presentation.models.forms.libraire;

import fr.orsys.formation.presentation.models.entities.Adresse;
import fr.orsys.formation.presentation.models.entities.Libraire;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Data
@Builder
public class FLibraireCreate {

    @NotBlank(message = "le nom est obligatoire")
    private String nom;
    @NotBlank(message = "La rue est obligatoire")
    private String adresseRue;
    @NotBlank(message = "La ville est obligatoire")
    private String adresseVille;
    @Min(value = 1, message = "Le premier numéro d'une rue c'est 1")
    private int adresseNumero;

    public static FLibraireCreate fromBll(Libraire bll) {
        return FLibraireCreate.builder()
                .nom(bll.getNom())
                .adresseRue(bll.getAdresse().getRue())
                .adresseNumero(Integer.valueOf(bll.getAdresse().getNumero()))
                .adresseVille(bll.getAdresse().getVille())
                .build();
    }

    public Libraire toBll() {
        Adresse adresse = Adresse.builder()
                .rue(adresseRue)
                .ville(adresseVille)
                .numero(String.valueOf(adresseNumero))
                .build();

        return Libraire.builder()
//                .nom(nom)
                .adresse(adresse)
                .build();
    }
}
