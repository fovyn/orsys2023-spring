package fr.orsys.formation.presentation.aspects;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
public class SuperviserAspect {

    @Around(value = "@annotation(supervision)")
    public Object superviser(ProceedingJoinPoint joinPoint, Supervision supervision) throws Throwable {
        long maxTime = supervision.timeInMillis();
        long startTime = System.currentTimeMillis();

        try {
            return joinPoint.proceed(joinPoint.getArgs());
        } finally {
            long endTime = System.currentTimeMillis();
            long processTime = endTime - startTime;

            if (processTime > maxTime) {
                log.warn("[{}] spent too much time => {}", joinPoint.toShortString(), processTime);
            }
        }
    }
}
