package fr.orsys.formation.presentation.controllers;

import fr.orsys.formation.presentation.models.entities.Livre;
import fr.orsys.formation.presentation.services.livre.LivreService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path = {"/livre"})
public class LivreController {

    private final LivreService livreService;

    public LivreController(LivreService livreService) {
        this.livreService = livreService;
    }

    @GetMapping(path = {"/{isbn}"})
    public String detailAction(
            Model model,
            @PathVariable(name = "isbn") String isbn
    ) {
        Livre livre = livreService.readOne(isbn).orElseThrow();

        livre.getRecompenses().forEach(System.out::println);


        return "livre/detail";
    }
}
