package fr.orsys.formation.presentation;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class PresentationApplication {


    public static void main(String[] args) {
        SpringApplication.run(PresentationApplication.class, args);
    }
}
