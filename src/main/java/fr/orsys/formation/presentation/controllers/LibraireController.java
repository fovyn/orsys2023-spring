package fr.orsys.formation.presentation.controllers;

import fr.orsys.formation.presentation.models.entities.Libraire;
import fr.orsys.formation.presentation.models.forms.libraire.FLibraireCreate;
import fr.orsys.formation.presentation.models.viewmodels.libraire.LibraireListVM;
import fr.orsys.formation.presentation.services.libraire.LibraireService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(path = {"/libraire"})
public class LibraireController {

    private final LibraireService service;

    @Autowired
    public LibraireController(LibraireService service) {
        this.service = service;
    }

    @GetMapping(path = {"", "/", "/list"})
    public String listAction(Model model) {
        List<Libraire> list = service.readAll().toList();

        model.addAttribute("model", LibraireListVM.builder().list(list).build());

        return "libraire/list";
    }

    @GetMapping(path = {"/add", "/create"})
    public String createAction(Model model) {

        return "libraire/create";
    }

    @PostMapping(path = {"/add", "/create"})
    public String createAction(Model model, @Valid FLibraireCreate form, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("errors", bindingResult.getFieldErrors());
            return "libraire/create";
        }
        service.create(form.toBll());
        return "redirect:/libraire";
    }

    @GetMapping(path = {"/update/{id:[0-9]+}"})
    public String updateAction(
            Model model,
//            @RequestParam(name = "id") int id => /update?id=X
            @PathVariable(name = "id") int id // => /update/283
    ) {
        Optional<Libraire> opt = service.readOne(id);

        opt.orElseThrow(() -> new RuntimeException());

        model.addAttribute("form", FLibraireCreate.fromBll(opt.get()));

        return "libraire/create";
    }
}
