package fr.orsys.formation.presentation.services.commande;

import fr.orsys.formation.presentation.models.entities.Commande;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.stream.Stream;

@Service
public class CommandeServiceImpl implements CommandeService {
    @Override
    public Commande create(Commande entity) {
        return null;
    }

    @Override
    public Optional<Commande> readOne(Long aLong) {
        return Optional.empty();
    }

    @Override
    public Stream<Commande> readAll() {
        return null;
    }

    @Override
    public Commande update(Long aLong, Commande entity) {
        return null;
    }

    @Override
    public void delete(Commande entity) {

    }

    @Override
    public void delete(Long aLong) {

    }
}
