package fr.orsys.formation.presentation.models.entities;

import lombok.*;

import java.io.Serializable;

@RequiredArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
//@Builder
public class Ecris implements Serializable {
    @Getter
    @Setter
    private Integer livreIsbn;

    @Getter
    @Setter
    private float prc;
}
