package fr.orsys.formation.presentation.models.viewmodels.libraire;

import fr.orsys.formation.presentation.models.entities.Libraire;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LibraireListVM {
    private List<Libraire> list = new ArrayList<>();

    public int size() {
        return list.size();
    }
}
