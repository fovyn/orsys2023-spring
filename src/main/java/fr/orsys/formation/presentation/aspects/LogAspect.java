package fr.orsys.formation.presentation.aspects;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.stream.Collectors;

//@Aspect
//@Component
//@Slf4j
//public class LogAspect {
//
//    @Before(value = "args()public * fr.orsys.formation.presentation.services.*.*(..))")
//    public void log(JoinPoint joinPoint) {
//        log.info("{}({})",
//                joinPoint.toShortString(),
//                Arrays.stream(joinPoint.getArgs()).map(Object::toString).collect(Collectors.joining(", "))
//                );
//    }
//}
