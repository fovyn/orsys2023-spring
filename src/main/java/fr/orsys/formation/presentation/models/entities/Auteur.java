package fr.orsys.formation.presentation.models.entities;

import lombok.*;

import javax.persistence.Entity;
import java.io.Serializable;

@RequiredArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
@NoArgsConstructor
//@Builder
@Entity
public class Auteur extends Personne implements Serializable {
    @NonNull
    @Getter
    @Setter
    private String prenom;
    @Getter
    @Setter
    private String pseudo;

//    @Getter
//    @Setter
////    @Singular(value = "ecris")
//    private List<Ecris> ecris;
}
