package fr.orsys.formation.presentation.models.entities;

import lombok.*;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@RequiredArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
//@Builder
public class Commande implements Serializable {
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    @Enumerated(EnumType.STRING)
    private CommandState etat = CommandState.EnPreparation;

    @Getter
    @Setter
    private LocalDate date = LocalDate.now();

    //    @Singular
    @Getter
    @Setter
    private List<CommandeEdition> details;
}
