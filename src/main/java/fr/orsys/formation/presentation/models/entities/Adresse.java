package fr.orsys.formation.presentation.models.entities;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@EqualsAndHashCode
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class Adresse implements Serializable {
    @Getter
    @Setter
    @Column(name = "ad_rue")
    private String rue;
    @Getter
    @Setter
    @Column(name = "ad_numero")
    private String numero;
    @Getter
    @Setter
    private String ville;
    @Getter
    @Setter
    private String codePostal;
    @Getter
    @Setter
    private String pays;
}
